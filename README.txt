This program 'anonymizes' a JIRA XML backup, replacing characters in
issues and projects with x's.

Usage
-----

java -jar joost.jar <backup.xml> anon.stx > anon-backup.xml

where '<backup.xml>' is your JIRA backup file



Invalid XML?
------------

If you encounter an error like:

jirabackup.xml:262393:50: An invalid XML character (Unicode: 0x1a) was found in the CDATA section.

then you'll have to first download the data cleaner utility, and use it
to remove invalid characters:

http://confluence.atlassian.com/display/JIRA/Removing+invalid+characters+from+XML+backups
