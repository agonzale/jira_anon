# Make clean backup

```
mv entities.xml old-entities.xml
java -jar $HOME/jira_anon/atlassian-xml-cleaner-0.1.jar old-entities.xml > entities-clean.xml
java -DentityExpansionLimit=2147480000 -DtotalEntitySizeLimit=2147480000 -Djdk.xml.totalEntitySizeLimit=2147480000 -Xmx2g -jar $HOME/jira_anon/joost.jar entities-clean.xml $HOME/jira_anon/anon.stx > entities.xml
zip clean-backup.zip entities.xml activeobjects.xml
```
